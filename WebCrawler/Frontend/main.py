from flask import Flask, request, render_template
import WebCrawler.spiders.My_web_spider as spider
import subprocess
from urllib.parse import urlparse
import json

app = Flask(__name__, template_folder='templates')


@app.route('/')
def home():
    return render_template('home.html')


#database = {'Himanshu': '123', 'Aditya': '456'}


#@app.route('/form_login', methods=['POST', 'GET'])
#def login():
#    name1 = request.form['username']
#    pwd = request.form['password']
#    if name1 not in database:
#        return render_template('login_page.html', info='Invalid User')
#    else:
#        if database[name1] != pwd:
#            return render_template('login_page.html', info='Invalid Password')
#        else:
#            return render_template('home.html', name=name1)


@app.route('/send_data', methods=['POST'])
def get_data_from_html():

    url = request.form['url']
    spider.MySpider.start_urls = url
    file = open("config.txt", "w")
    file.write(url)
    file.close()
    print("url is " + url)

    if url:
        filename = urlparse(url).netloc
        json_name = './Crawled-Data/'+filename+'.json'
        cmd = 'scrapy crawl MySpider -O'+json_name
        p = subprocess.Popen(cmd, shell=True)
        out, err = p.communicate()
        print(err)
        print(out)
        f = open('./Crawled-Data/'+filename+'.json')
        c = open("crawlspeed.txt", "r")
        try:
            crawl_speed = c.read()
        except:
            crawl_speed = 'unknown'
        try:
            data = json.load(f)
        except:
            return 'something went wrong while loading data, please specify url correctly(i.e. with www and http://)'
        if data:
            return render_template('result.html', data=data, crawl_speed=crawl_speed)
        else:
            return 'something went wrong'
    else:
        return 'something went wrong'


if __name__ == '__main__':
    app.run(port=3000)
