import scrapy
from WebCrawler.items import WebcrawlerItem


class MySpider(scrapy.spiders.Spider):
    # print(main.url)
    name = "MySpider"

    def parse(self, response):

        items = WebcrawlerItem()
        try:
            title = response.css('title ::text').extract()
        except:
            title = 'data not found, please try with different url'
        try:
            description = response.xpath("//meta[@name='Description']/@content")[0].extract()
        except:
            try:
                description = response.xpath("//meta[@name='description']/@content")[0].extract()
            except:
                description = 'no data found'
        try:
            links = response.xpath("//a/@href").extract()
        except:
            links = 'data not found'

        items['title'] = title
        items['description'] = description
        items['links'] = links

        # for link in links:
        #   if 'geeksforgeeks' in link:
        #      print('geeksforgeeks present here' + link)

        yield items

    def start_requests(self):
        f = open("../Frontend/config.txt", "r")
        url = f.read()
        # Set the headers here
        headers = {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.84 Safari/537.36'
        }
        yield scrapy.http.Request(url, headers=headers)

    def close(self, reason):
        start_time = self.crawler.stats.get_value('start_time')
        finish_time = self.crawler.stats.get_value('finish_time')
        total_time = finish_time - start_time
        total_time = str(total_time)
        file = open("crawlspeed.txt", "w")
        file.write(total_time)
        file.close()
